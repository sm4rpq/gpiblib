/*
Copyright 2022 Leif Holmgren, SM4RPQ

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ss80.h"
#include "ibcontroller.h"

SS80::SS80(IBCONTROLLER* interface)
{
  m_pInterface=interface;
  m_bSetUnit=false;
  m_bSetVolume=false;
  m_bSetAddress=false;
}

SS80::~SS80()
{
  
}

void SS80::setDevice(uint8_t device)
{
  m_uiDevice = device;
  m_uiDeviceMask = 2^device;
}

void SS80::setUnit(uint8_t unit)
{
  m_bSetUnit = true;
  m_uiUnit = unit;
}

void SS80::setVolume(uint8_t volume)
{
  m_bSetVolume = true;
  m_uiVolume = volume;
}

void SS80::setAddress(uint32_t address)
{
  m_bSetAddress = true;
  m_uiAddress = address;
}

uint8_t SS80::identify()
{
  char idPacket[4];
  unsigned int rxLen;
  m_pInterface->read(31,m_uiDevice,idPacket,4,rxLen,true,false,false);
  if (rxLen!=2)
  {
    Serial.println("Unexpected length of data received");
    return 0;
  }
  else
  {
    return (int)(idPacket[1]);
  }
}

uint8_t SS80::describe(char pBuf[], unsigned int nBufSize, unsigned int & nChars)
{
  if (nBufSize<36)
    return -1;
    
  char cmd[8];
  unsigned int nCmdSize=0;
  if (m_bSetUnit)
  {
    cmd[nCmdSize]=(char)(0x20+m_uiUnit);
    nCmdSize++;
    m_bSetUnit=false;
  }
  if (m_bSetVolume)
  {
    cmd[nCmdSize]=(char)(0x40+m_uiVolume);
    nCmdSize++;
    m_bSetVolume=false;
  }
  cmd[nCmdSize]=(char)(0x35);
  nCmdSize++;
  command(cmd,nCmdSize);
  executeRead(pBuf,nBufSize,nChars);
  uint8_t code=report();
  if (code>0)
  {
    requestStatus();
  }
  return code;
}

void SS80::initializeMedia()
{
  char cmd[6];
  cmd[0]=(char)(0x20+m_uiUnit); //Set unit
  cmd[1]=(char)(0x40+m_uiVolume); //Set volume
  cmd[2]=(char)(0x37); //Initialize media
  cmd[3]=(char)(0x02); //Retain no spares
  cmd[4]=(char)(0x01); //Interleave factor 1
  command(cmd,5);
  report();
}

uint8_t SS80::read(char pBuf[], unsigned int nBufSize, unsigned int & nSize)
{
  char cmd[20];
  unsigned int nCmdSize=0;
  if (m_bSetUnit)
  {
    cmd[nCmdSize++]=(char)(0x20+m_uiUnit);
    m_bSetUnit=false;
  }
  if (m_bSetVolume)
  {
    cmd[nCmdSize++]=(char)(0x40+m_uiVolume);
    m_bSetVolume=false;
  }
  if (m_bSetAddress)
  {
    cmd[nCmdSize++]=(char)(0x10); //Set address
    cmd[nCmdSize++]=(char)(0x00); //Set address
    cmd[nCmdSize++]=(char)(0x00); //Set address
    cmd[nCmdSize++]=(char)((m_uiAddress>>24)&0xff);
    cmd[nCmdSize++]=(char)((m_uiAddress>>16)&0xff);
    cmd[nCmdSize++]=(char)((m_uiAddress>>8)&0xff);
    cmd[nCmdSize++]=(char)(m_uiAddress&0xff);
    m_bSetAddress=false;
  }
  cmd[nCmdSize++]=(char)(0x18); //Set length
  cmd[nCmdSize++]=(char)((nSize>>24)&0xff);
  cmd[nCmdSize++]=(char)((nSize>>16)&0xff);
  cmd[nCmdSize++]=(char)((nSize>>8)&0xff);
  cmd[nCmdSize++]=(char)(nSize&0xff);

  cmd[nCmdSize++]=(char)(0x00); //Locate and read
  command(cmd,nCmdSize);
  executeRead(pBuf,nBufSize,nSize);
  uint8_t result=report();
  if (result>0)
    requestStatus();
  return result;
}

uint8_t SS80::write(char pBuf[], unsigned int nSize)
{
  char cmd[20];
  unsigned int nCmdSize=0;
  if (m_bSetUnit)
  {
    cmd[nCmdSize++]=(char)(0x20+m_uiUnit);
    m_bSetUnit=false;
  }
  if (m_bSetVolume)
  {
    cmd[nCmdSize++]=(char)(0x40+m_uiVolume);
    m_bSetVolume=false;
  }
  if (m_bSetAddress)
  {
    cmd[nCmdSize++]=(char)(0x10); //Set address
    cmd[nCmdSize++]=(char)(0x00); //Set address
    cmd[nCmdSize++]=(char)(0x00); //Set address
    cmd[nCmdSize++]=(char)((m_uiAddress>>24)&0xff);
    cmd[nCmdSize++]=(char)((m_uiAddress>>16)&0xff);
    cmd[nCmdSize++]=(char)((m_uiAddress>>8)&0xff);
    cmd[nCmdSize++]=(char)(m_uiAddress&0xff);
    m_bSetAddress=false;
  }
  cmd[nCmdSize++]=(char)(0x18); //Set length
  cmd[nCmdSize++]=(char)((nSize>>24)&0xff);
  cmd[nCmdSize++]=(char)((nSize>>16)&0xff);
  cmd[nCmdSize++]=(char)((nSize>>8)&0xff);
  cmd[nCmdSize++]=(char)(nSize&0xff);

  cmd[nCmdSize++]=(char)(0x02); //Locate and read
  command(cmd,nCmdSize);
  executeWrite(pBuf,nSize);
  uint8_t result=report();
  if (result>0)
    requestStatus();
  return result;
}

uint8_t SS80::requestStatus()
{
  char cmd[3];
  cmd[0]=(char)(0x20+m_uiUnit);
  cmd[1]=(char)(0x0d);
  command(cmd,2);
  executeRead(m_status,24,m_nStatusSize);
  uint8_t result=report();

  if ((m_status[2]&0x20)==0x20) //2
  {
    Serial.println(" Drive reports: Channel Parity Error");
  }
  if ((m_status[2]&0x04)==0x04) //5
  {
    Serial.println(" Drive reports: Illegal Opcode");
  }
  if ((m_status[2]&0x02)==0x02) //6
  {
    Serial.println(" Drive reports: Module Addressing");
  }
  if ((m_status[2]&0x01)==0x01) //7
  {
    Serial.println(" Drive reports: Address Bounds");
  }
  if ((m_status[3]&0x80)==0x80) //8
  {
    Serial.println(" Drive reports: Parameter Bounds");
  }
  if ((m_status[3]&0x40)==0x40) //9
  {
    Serial.println(" Drive reports: Illegal Parameter");
  }
  if ((m_status[3]&0x20)==0x20) //10
  {
    Serial.println(" Drive reports: Message Sequence");
  }
  if ((m_status[3]&0x08)==0x08) //12
  {
    Serial.println(" Drive reports: Message Length");
  }
  if ((m_status[4]&0x10)==0x10) //19
  {
    Serial.println(" Drive reports: Controller Fault");
  }
  if ((m_status[4]&0x02)==0x02) //22
  {
    Serial.println(" Drive reports: Unit Fault");
  }
  if ((m_status[5]&0x80)==0x80) //24
  {
    Serial.println(" Drive reports: Diagnostic Result");
    for (uint8_t p=10; p<19; p++)
    {
      Serial.print(m_status[p],HEX);
      Serial.print(" ");
    }
    Serial.println();
  }
  if ((m_status[5]&0x02)==0x02) //30
  {
    Serial.println(" Drive reports: Power Fail");
  }
  if ((m_status[5]&0x01)==0x01) //31
  {
    Serial.println(" Drive reports: Re-transmit");
  }
  if ((m_status[6]&0x40)==0x40) //33
  {
    Serial.println(" Drive reports: Uninitialized media");
  }
  if ((m_status[6]&0x20)==0x20) //34
  {
    Serial.println(" Drive reports: No Spares Available");
  }
  if ((m_status[6]&0x10)==0x10) //35
  {
    Serial.println(" Drive reports: Not Ready");
  }
  if ((m_status[6]&0x08)==0x08) //36
  {
    Serial.println(" Drive reports: Write Protect");
  }
  if ((m_status[6]&0x04)==0x04) //37
  {
    Serial.println(" Drive reports: No Data Found");
  }
  if ((m_status[7]&0x80)==0x80) //40
  {
    Serial.println(" Drive reports: Unrecoverable Data Overflow");
  }
  if ((m_status[7]&0x40)==0x40) //41
  {
    Serial.println(" Drive reports: Unrecoverable Data");
  }
  if ((m_status[7]&0x10)==0x10) //43
  {
    Serial.println(" Drive reports: End of File");
  }
  if ((m_status[7]&0x08)==0x08) //44
  {
    Serial.println(" Drive reports: End of Volume");
  }
  
  return result;
}

void SS80::getStatus(char buf[], unsigned int nBufSize, unsigned int & nSize)
{
  for(uint8_t i=0; (i<m_nStatusSize)&&(i<nBufSize); i++)
    buf[i]=m_status[i];
  nSize = m_nStatusSize;
}

void SS80::amigoClear()
{
  char cmd[2];
  cmd[0]=(char)(0);
  m_pInterface->write(m_uiDevice,16,cmd,2,true);
  m_pInterface->dcl(m_uiDevice,16);
  //Repeat ppoll until set
  while ((m_pInterface->ppoll()&0xff)==0) //TODO: Use m_uiDeviceMask
  {
    Serial.println("SS80report::Poll not set");
    delay(20);
  }
}

uint8_t SS80::channelIndepClear()
{
  Serial.println("====SS80::Channel Independent Clear====");
  char cmd[3];
  cmd[0]=(char)(0x2f);
  cmd[1]=(char)(0x80);
  command(cmd,2);
  uint8_t result=report();
  if (result>0)
    requestStatus();
  return result;
}

uint8_t SS80::initDiagnostic()
{
  char cmd[5];
  unsigned int nCmdSize = 0;
  if (m_bSetUnit)
  {
    cmd[nCmdSize++]=(char)(0x20+m_uiUnit); //Set unit 0
    m_bSetUnit=false;
  }
  cmd[nCmdSize++]=(char)(0x33);
  cmd[nCmdSize++]=(char)(0x00);
  cmd[nCmdSize++]=(char)(0x01);
  cmd[nCmdSize++]=(char)(0x00); 
  command(cmd,nCmdSize);
  uint8_t result=report();
  if (result>0)
  {
    requestStatus();
  }
  return result;
}

void SS80::command(char cmd[], unsigned int nCmdSize)
{
  bool bOk = m_pInterface->write(m_uiDevice,0x05,cmd,nCmdSize,true);
  if (!bOk)
    Serial.println("Error on write command");
}

void SS80::executeRead(char pBuf[], unsigned int nBufSize, unsigned int & nChars)
{
  //Repeat ppoll until set
  while ((m_pInterface->ppoll()&0xff)==0) // TODO: Use m_uiDeviceMask
    delay(20);

  //read from device
  m_pInterface->read(m_uiDevice,0x0e,pBuf,nBufSize,nChars,true,false,false);
}

void SS80::executeWrite(char pBuf[], unsigned int nChars)
{
  //Repeat ppoll until set
  while ((m_pInterface->ppoll()&0xff)==0) //TODO: Use m_uiDeviceMask
    delay(20);

  //write to device
  m_pInterface->write(m_uiDevice,0x0e,pBuf,nChars,true);
}

uint8_t SS80::report()
{
  //Repeat ppoll until set
  while ((m_pInterface->ppoll()&0xff)==0) //TODO: Use m_uiDeviceMask
    delay(20);

  //Read report
  char buf[2];
  unsigned int nBufSize;
  m_pInterface->read(m_uiDevice,0x10,buf,2,nBufSize,true,false,false);
  if (nBufSize!=1)
  {
    Serial.println("Unexpected length of report");
    return 3;
  }
  else
  {
    uint8_t code=(uint8_t)(buf[0]);
    if (code>0)
    {
      Serial.print("  QStat=");
      Serial.println(code);
    }
    return code;
  }
}

