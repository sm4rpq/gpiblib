/*
Copyright 2022 Leif Holmgren, SM4RPQ

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <arduino.h>

class IBCONTROLLER;
class IBNODE
{
  public:
    IBNODE(IBCONTROLLER* interface);
    ~IBNODE();

    void setTxEOI(bool bUseEOI); //true => EOI set on last character transmitted before unlisten
    void setRxEOI(bool bUseEOI); //true => when EOI is set reception is terminated
    
    void setTxTerm(bool bAppendCR, bool bAppendLF); // => Potentially add CR and/or LF to transmission
    void setRxTerm(bool bTermOnCR, bool bTermOnLF); // => Terminate reception on CR and/or LF. NB! If both are received the first one terminates so set carefully

    //Set current device address.
    //If secondary addressing is to be used, be sure to set both primary and secondary address.
    //If just primary addressing is used, use default value for secondary address
    void setAddr(uint8_t iPrimary, uint8_t iSecondary=31);

    //Writes text string from pBuf to the current device. Will do all the listen/unlisten needed and
    //will add cr/lf if specified as terminators
    bool write(char pBuf[]);

    //Writes byte array from pBuf to the current device. Will NOT add any characters and can send null
    //characters if inside the buffer.
    bool write(char pBuf[],unsigned int nChars);

    //Reads byte arrat from current device to pBuf until the terminator is found. Any CR/LF terminators
    //will be included in the returned buffer.
    bool read(char pBuf[], unsigned int nBufSize, unsigned int & nChars);
    
  private:
    bool m_bTxEOI;
    bool m_bRxEOI;
    bool m_bAppendCR;
    bool m_bAppendLF;
    bool m_bTermOnCR;
    bool m_bTermOnLF;
    uint8_t m_uiPrimaryAddr;
    uint8_t m_uiSecondaryAddr;

    IBCONTROLLER* m_pController;
};

