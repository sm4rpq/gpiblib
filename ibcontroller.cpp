/*
Copyright 2022 Leif Holmgren, SM4RPQ

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ibcontroller.h"

const int RENpin = 9;
const int IFCpin = 10;
const int NDACpin = 11;
const int NRFDpin = 12;
const int TEpin = 13;
const int DAVpin = 15;
const int SRQpin = 16;
const int PEpin = 17;
const int DCpin = 18;
const int EOIpin = 22;
const int ATNpin = 23;

const int assert = 0;
const int unassert = 1;

/* 
 * Constructor, sets up pins as digital inputs/outputs and initializes some local variables.
 * The controller will be in a passive state after call.
 */

IBCONTROLLER::IBCONTROLLER()
{
  //All pins on the teensy default to inputs. Must set 0 and 1 BEFORE turning pins to avoid output to output connection

  //TE, DC and PE are always inputs on the drivers to control their direction

  //PE = Pull-Up Enable
  pinMode(PEpin,OUTPUT);  //Pullup Enable
  digitalWrite(PEpin,1);  //Enable tri-state drivers

  //DC = Direction Control (or domain controller...)
  pinMode(DCpin,OUTPUT);  //Direction Control
  digitalWrite(DCpin,0);  //Direction control=0 => ATN/REN/IFC = outputs, SRQ=Input

  pinMode(TEpin,OUTPUT); //Talk Enable
  
  pinMode(RENpin,OUTPUT);  //Remote enable
  pinMode(ATNpin,OUTPUT);  //Attention
  pinMode(IFCpin,OUTPUT);  //Interface clear
  
  pinMode(SRQpin,INPUT);  //Service Request

  m_bAttention = false;
  setInput();

  //These pins are always outputs since we are the controller
  digitalWrite(RENpin,unassert); // Clear remote enable
  digitalWrite(ATNpin,unassert); // Clear attention
  digitalWrite(IFCpin,unassert); // Clear interface clear

  m_uiTimeout=5000;
}

IBCONTROLLER::~IBCONTROLLER()
{
  
}

/*
 * Public functions for communication over GPIB. None are meant to be used from outside the interface classes.
 * Instead use the facade class for interface wide functions and the channel class for address specific
 * functions.
 */

/*
 * Begin is used to assert remote enable and pulse interface clear.
 */
 
void IBCONTROLLER::begin()
{
  digitalWrite(RENpin,assert);
  digitalWrite(IFCpin,assert);
  delayMicroseconds(200);
  digitalWrite(IFCpin,unassert);
}

/*
 * End is used to deassert remote enable again
 */

void IBCONTROLLER::end()
{
  digitalWrite(RENpin,unassert);
}

/*
 * Performe a parallell poll.
 * That is done by asserting attention (ATN) and EOI, which in that case becomes Identify.
 */

uint8_t IBCONTROLLER::ppoll()
{
  setInput();
  setAttention(true);
  digitalWrite(EOIpin,assert);
  delayMicroseconds(2);
  uint8_t temp = (uint8_t)(GPIOD_PDIR & 0xFF);
  digitalWrite(EOIpin,unassert);
  setAttention(false);
  return ~temp & 0xFF;
}

/*
 * Device Clear
 */
void IBCONTROLLER::dcl(unsigned int uiPrimaryAddress, unsigned int uiSecondaryAddress)
{
  setAttention(true);
  setOutput();
  wbyte(uiPrimaryAddress+32); //Address the device as listener
  if (uiSecondaryAddress<31)
    wbyte(uiSecondaryAddress+96);
  wbyte(4);
  wbyte(31+32); //Unlisten
  setAttention(false);  
}

/*
 * Read status of service request
 */

bool IBCONTROLLER::srq()
{
  return digitalRead(SRQpin)==assert;
}

/* 
 *  High level write function. Executes the signalling needed to send a sequence of bytes over the bus.
 *  The signalling in this case is multiline messages, so listen adressing (primary and secondary if relevant)
 *  and the byte sequence.
 */

bool IBCONTROLLER::write(unsigned int uiPrimaryAddress, unsigned int uiSecondaryAddress, char pBuf[], unsigned int nChars, bool bSetEOI, bool bAddCR, bool bAddLF)
{
  setAttention(true);
  setOutput();
  bool bOk;
  bOk = wbyte(uiPrimaryAddress+32);
  if ((uiSecondaryAddress!=31) && bOk)
    bOk = wbyte(uiSecondaryAddress+96);
  setAttention(false);
  bool bEOIOnLast = bSetEOI && (!(bAddLF || bAddCR));
  for(unsigned int i=0; (i<nChars && bOk); i++)
  {
    bOk = wbyte(pBuf[i],(i==(nChars-1)&&(bEOIOnLast)));
  }
  if (bAddCR)
    wbyte(13,(!bAddLF) && bSetEOI);
  if (bAddLF)
    wbyte(10,bSetEOI);
  setAttention(true);
  wbyte(31+32);  //UNLISTEN
  setAttention(false);
  setInput();  
  return bOk;
}

/*
 * High level read function. Executes the signalling needed to receive a sequence of bytes over the bus.
 * The signalling in this case is multiline messages, so talk adressing (primary and secondary if relevant)
 * and getting bytes until termination condition is reached.
 * Exact break conditions is specified as a combo of EOI, CR, LF and timeout.
 */

bool IBCONTROLLER::read(unsigned int uiPrimaryAddress, unsigned int uiSecondaryAddress, char pBuf[], unsigned int nBufSize, unsigned int& nChars, bool bBreakOnEOI, bool bBreakOnCR, bool bBreakOnLF)
{
  if (nBufSize==0)
    return false;
  setAttention(true);
  delayMicroseconds(10);
  setOutput();
  wbyte(uiPrimaryAddress+64);
  if (uiSecondaryAddress!=31)
    wbyte(uiSecondaryAddress+96);
  setAttention(false);
  setInput();

  char c;
  bool e = false;
  unsigned int p=0;
  while (!e)
  {
    bool bOk=rbyte(c,e); //Reads a byte and returns it + eoi status
    if (!bBreakOnEOI) e=false; //Check if eoi to be ignored or used
    if (bBreakOnLF && (c==0x0a)) e=true; //Break on line feed?
    if (bBreakOnCR && (c==0x0d)) e=true; //Break on carriage return?
    if (bOk)
    {
      pBuf[p]=c;
      p++;
    }
  }
  nChars=p;
  pBuf[p]=0; //Ensure null termination
  
  setAttention(true);
  delayMicroseconds(200);
  setOutput();
  wbyte(31+64);  //UNTALK
  setAttention(false);

  setInput();
  return true;  
}

/*
 * Lower level functions for the interface.
 * The functions following are closer to the hardware. THat is, controlling actual signal lines
 * by altering between inputs/outputs and doing some handshake.
 */

/*  
 * Turns the interface lines to outputs.
 * DIO-lines set as out
 * DAV,EOI set as out
 * NDAC,NRFD set as in
 * Has to control things in the correct order to avoid making driver outputs connected to MCU outputs.
 */
 
void IBCONTROLLER::setOutput()
{
  m_bTalker = true;
  //First turn bidirectional pins that should be inputs to inputs
  pinMode(NDACpin,INPUT);
  pinMode(NRFDpin,INPUT);

  //Then turn buffers (all pins now inputs=>No output to output pins)
  digitalWrite(TEpin,1);

  //Finally turn pins that should be outputs to outputs
  pinMode(DAVpin,OUTPUT);
  pinMode(EOIpin,OUTPUT);

  pinMode(2,OUTPUT);  //D0
  pinMode(14,OUTPUT); //D1
  pinMode(7,OUTPUT);  //D2
  pinMode(8,OUTPUT);  //D3
  pinMode(6,OUTPUT);  //D4
  pinMode(20,OUTPUT); //D5
  pinMode(21,OUTPUT); //D6
  pinMode(5,OUTPUT);  //D7
}

/*  
 * Turns the interface lines to inputs.
 * DIO-lines set as in
 * DAV,EOI set as in
 * NDAC,NRFD set as out
 * Has to control things in the correct order to avoid making driver outputs connected to MCU outputs.
 */

void IBCONTROLLER::setInput()
{
  m_bTalker = false;
  //First turn bidirectional pins that should be inputs to inputs
  pinMode(DAVpin,INPUT);
  pinMode(EOIpin,INPUT);

  pinMode(2,INPUT);  //D0
  pinMode(14,INPUT); //D1
  pinMode(7,INPUT);  //D2
  pinMode(8,INPUT);  //D3
  pinMode(6,INPUT);  //D4
  pinMode(20,INPUT); //D5
  pinMode(21,INPUT); //D6
  pinMode(5,INPUT);  //D7

  //Then turn buffers
  digitalWrite(TEpin,0);

  //Finally turn pins that should be outputs to outputs
  digitalWrite(NDACpin,assert);
  pinMode(NDACpin,OUTPUT);
  digitalWrite(NRFDpin,assert);
  pinMode(NRFDpin,OUTPUT);

  if (m_bAttention)
    pinMode(EOIpin,OUTPUT);

}

/*  
 * Set the state of the attention pin.
 * Also alters the direction of the EOI pin to match
 * If ATN is asserted EOI is an output
 * If ATN is unasserted EOI follows talk/listen
 */

void IBCONTROLLER::setAttention(bool b)
{
  m_bAttention=b;
  if (b)
  {
    //Asserting attention also means EOI turns to an output
    digitalWrite(ATNpin,assert);
    pinMode(EOIpin,OUTPUT);
  }
  else
  {
    //unasserting attention also means EOI turn to follow TE state
    // At talk, EOI is output
    // At listen, EOI is input
    pinMode(EOIpin,INPUT);
    digitalWrite(ATNpin,unassert);
    if (m_bTalker)
      pinMode(EOIpin,OUTPUT);
  }
}

/*
 * Does a DIO message transmit handshake. Essentially a loop around the SH statechart
 */
bool IBCONTROLLER::wbyte(byte b, bool eoi)
{
  unsigned int start;
  bool bTimeout = false;

  digitalWrite(DAVpin,unassert);

  //SGNS
  start = millis();
  while ((digitalRead(NRFDpin)!=assert) && (digitalRead(NDACpin)!=assert) && !bTimeout)
  {
    bTimeout=millis()>start+m_uiTimeout;
  }
  if (bTimeout)
  {
    Serial.println("    Timeout. No listeners");
    return false;
  }

  if (eoi)
    digitalWrite(EOIpin,assert);
  else
    digitalWrite(EOIpin,unassert);
  //Place data on data lines

  GPIOD_PDOR = (GPIOB_PDOR & 0xFFFFFF00) + (~b & 0XFF);

  //SDYS
  delayMicroseconds(1); //Allow data lines to settle

  //Make sure NRFD is unasserted before continuing
  start = millis();
  bTimeout = false;
  while ((digitalRead(NRFDpin)==assert) && !bTimeout)
  {
    bTimeout=(millis()>start+m_uiTimeout);
  };
  if (bTimeout)
  {
    Serial.println("  Timeout waiting for NRFD=unasserted");
    return false;
  }

  //STRS
  digitalWrite(DAVpin,assert);

  start = millis();
  bTimeout = false;
  while ((digitalRead(NDACpin)==assert) && !bTimeout)
  {
    bTimeout=(millis()>start+m_uiTimeout);
  };
  if (bTimeout)
  {
    Serial.println("  Timeout waiting for NDAC=unasserted");
    return false;
  }

  //SWNS
  digitalWrite(DAVpin,unassert);
  delayMicroseconds(1);

  return true;
}

/*
 * Does a DIO message receive handshake. Essentially a loop around the AH statechart
 */
bool IBCONTROLLER::rbyte(char& rxByte, bool &rxEoi)
{
  //ACRS
  digitalWrite(NRFDpin,unassert);
  
  //Wait for DAV
  unsigned int start = millis();
  bool bTimeout = false;
  while ((digitalRead(DAVpin)==unassert) && !bTimeout)
  {
    bTimeout=(millis()>start+m_uiTimeout);
  };
  if (bTimeout)
  {
    Serial.println("  Timeout waiting for DAV asserted");
    return false;
  }

  //ACDS
  digitalWrite(NRFDpin,assert);
  rxByte = (~GPIOD_PDIR) & 0xFF;
  rxEoi = digitalRead(EOIpin)==assert;
  digitalWrite(NDACpin,unassert);

  //AWNS
  start = millis();
  bTimeout = false;
  while ((digitalRead(DAVpin)==assert) && !bTimeout)
  {
    bTimeout=(millis()>start+m_uiTimeout);
  };
  if (bTimeout)
  {
    Serial.println("  Timeout waiting for DAV unasserted");
    return false;
  }

  //ANRS
  digitalWrite(NDACpin,assert);

  return true;
}


