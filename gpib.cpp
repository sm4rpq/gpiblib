/*
 * Facade class for gpib functions
 * 
Copyright 2022 Leif Holmgren, SM4RPQ

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "gpib.h"
#include "ibcontroller.h"
#include "ibnode.h"
#include "ss80.h"

const int assert = 0;
const int unassert = 1;

GPIB::GPIB()
{
  ibcontrol = new IBCONTROLLER();
}

GPIB::~GPIB()
{
  delete ibcontrol;
}

void GPIB::begin()
{
  ibcontrol->begin();
}

void GPIB::end()
{
  ibcontrol->end();
}

IBNODE* GPIB::getInstrument()
{
  return new IBNODE(ibcontrol);
}

SS80* GPIB::getDisc()
{
  return new SS80(ibcontrol);
}

uint8_t GPIB::ppoll()
{
  return ibcontrol->ppoll();
}

bool GPIB::srq()
{
  return ibcontrol->srq();
}

