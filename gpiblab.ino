/*

This is just a example of how you can use the GPIB classes.
It does not currently use all of it. Use the conditional defines below to
select between working with a SS80 drive or some GPIB connected instruments.


==============

Copyright 2022 Leif Holmgren, SM4RPQ

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "gpib.h"
#include "ibnode.h"
#include "ss80.h"
#include <Entropy.h>

#define DISKTEST
//#define INSTTEST

GPIB gpib;

int tstart,tstop;

#ifdef DISKTEST
SS80* hpdir;

void setup() {
  while(!Serial)
  {
    ; //Wait for serial monitor
  }

  hpdir=gpib.getDisc();
  gpib.begin();
  hpdir->setDevice(0); //GPIB-address of drive
  Entropy.Initialize();
}

void Identify()
{
  uint8_t device = hpdir->identify();
  Serial.print("Device identifies as:");
  Serial.println(device);

  Serial.println("Description:");
  hpdir->setUnit(15);
  hpdir->setVolume(0);
  char dt[256];
  unsigned int nDt;
  hpdir->describe(dt,256,nDt);
  dump(dt,nDt);
  uint8_t descriptorOffset = 0;
  SS80_ControllerDescription* ctrlPtr = (SS80_ControllerDescription*)(dt+descriptorOffset);

  {
    Serial.println("Controller:");
    Serial.print(" Unit map...............: ");
    Serial.println(byteswap(ctrlPtr->installedUnits),HEX);

    Serial.print(" Transfer rate kb/s.....: ");
    Serial.println(byteswap(ctrlPtr->tfrRate));

    Serial.print(" Controller type........: ");
    switch (ctrlPtr->controllerType)
    {
      case 0: Serial.println("CS/80 integrated single unit controller"); break;
      case 1: Serial.println("CS/80 integrated multi-unit controller"); break;
      case 2: Serial.println("CS/80 integrated multi-port controller"); break;
      case 3: Serial.println("SS/80 integrated single unit controller"); break;
      case 4: Serial.println("SS/80 integrated multi-unit controller"); break;
      case 5: Serial.println("SS/80 integrated multi-port controller"); break;
      default: Serial.println("Unknown controller");
    }
    Serial.println();
    descriptorOffset = descriptorOffset + sizeof(SS80_ControllerDescription);

    /////////////// Loop over units for controller
    uint16_t unitBitField = (ctrlPtr->installedUnits[0]*256+ctrlPtr->installedUnits[1]) & 0x7FFF; //Mask out controller bit
    uint16_t unitMask = 1;
    for (uint8_t unit=0; (unitBitField & unitMask)!=0; unit++)
    {
      Serial.print(" Unit ");
      Serial.print(unit);
      Serial.println(":");
      
      SS80_UnitDescription* unitPtr = (SS80_UnitDescription*)(dt+descriptorOffset);

      Serial.print("  Generic unit type......: ");
      switch (unitPtr->genericUnitType)
      {
        case 0:   Serial.println("Fixed disc"); break;
        case 1:   Serial.println("Flexible disc"); break;
        case 2:   Serial.println("Tape"); break;
        case 129: Serial.println("Dumb"); break;
        default:  Serial.println("Unknown");
      }
      Serial.print("  Device Number..........: ");
      for (uint8_t b=0; b<3; b++)
        Serial.print(unitPtr->deviceNumber[b],HEX);
      Serial.println();

      Serial.print("  Bytes per block........: ");
      hpdir->m_uiBlockSize = byteswap(unitPtr->numBytesPerBlock); 
      Serial.println(hpdir->m_uiBlockSize);

      Serial.print("  Block time millis......: ");
      Serial.println(byteswap(unitPtr->blockTimeMillis));

      Serial.print("  Transfer Rate kb/s.....: ");
      Serial.println(byteswap(unitPtr->transferRatekBps));

      Serial.print("  Fixed Volume Byte......: ");
      Serial.println(unitPtr->fixedVolumeByte,HEX);
      
      Serial.print("  Removable Volume Byte..: ");
      Serial.println(unitPtr->removableVolumeByte,HEX);

      Serial.println(); //End of unit descriptor
      
      descriptorOffset = descriptorOffset + sizeof(SS80_UnitDescription);
      unitMask = unitMask << 1;

      ///////////////// Loop over Volumes for unit
      
      uint16_t volumeBitField;
      if(unitPtr->genericUnitType==0)
        volumeBitField = unitPtr->fixedVolumeByte;
      else
        volumeBitField = unitPtr->removableVolumeByte;
      uint16_t volumeMask = 1;
      for (uint8_t volume=0; (volumeBitField & volumeMask)!=0; volume++)
      {
        Serial.print("  Volume: ");
        Serial.println(volume);

        SS80_VolumeDescription* volPtr = (SS80_VolumeDescription*)(dt+descriptorOffset);

        Serial.print("   Max Head Address...........: ");
        Serial.println(volPtr->maxHeadAddress);
        Serial.print("   Max Sector Address.........: ");
        Serial.println(byteswap(volPtr->maxSectorAddress));

        Serial.print("   Max single vector adrdress.: ");
        hpdir->m_uiBlockCount = (uint8_t)(dt[descriptorOffset+10])*256+(uint8_t)(dt[descriptorOffset+11])+1;
        Serial.println(hpdir->m_uiBlockCount-1);
        
        
        Serial.print("   Current Interleave factor..: ");
        Serial.println(volPtr->currentInterleave);

        Serial.println(); //End of volume descriptor
        
        descriptorOffset = descriptorOffset + sizeof(SS80_VolumeDescription);
        volumeMask = volumeMask << 1;
      }
    }
  }
}

uint16_t byteswap(uint8_t p[2])
{
  return p[0]*256+p[1];
}

void Diagnostic()
{
  char st[24];
  unsigned int nSt;

  Serial.println("====  Diagnostic for unit 0");
  hpdir->setUnit(0);
  if (hpdir->initDiagnostic()!=0)
  {
    Serial.println("Unit 0 failed diagnostic");
    hpdir->getStatus(st,24,nSt);
    dump(st,nSt);
  }
  else
    Serial.println("Unit 0 diagnostic ok");

  Serial.println("====  Diagnostic for unit 1");
  hpdir->setUnit(1);
  if (hpdir->initDiagnostic()!=0)
  {
    Serial.println("Unit 1 failed diagnostic");
    hpdir->getStatus(st,24,nSt);
    dump(st,nSt);
  }
  else
    Serial.println("Unit 1 diagnostic ok");

/*
  Serial.println("Diagnostic 15");
  hpdir->setUnit(15);
  if (hpdir->initDiagnostic()!=0)
  {
    Serial.println("Some unit(s) failed diagnostic");
    hpdir->getStatus(s);
    dump(s);
  }
*/
}

void ClearQStat()
{
  char st[24];
  unsigned int nSt;
  char dt[256];
  unsigned int nDt;

  hpdir->setUnit(0);
  hpdir->setVolume(0);
  hpdir->setAddress(0);
  nDt=256;
  if (hpdir->read(dt,256,nDt)!=0)
  {
    Serial.println("Failed read on unit 0 with status:");
    hpdir->getStatus(st,24,nSt);
    dump(st,nSt);
  }

  hpdir->setUnit(1);
  hpdir->setVolume(0);
  hpdir->setAddress(0);
  nDt=256;
  if (hpdir->read(dt,256,nDt)!=0)
  {
    Serial.println("Failed read on unit 1 with status:");
    hpdir->getStatus(st,24,nSt);
    dump(st,nSt);
  }
}

void DumpDisc()
{
  char st[24];
  unsigned int nSt;
  char dt[256];
  unsigned int nDt;
  
  int unitToDump = 1;

  hpdir->setUnit(unitToDump);
  hpdir->setVolume(0);
  hpdir->setAddress(0);

  bool readOk = true;
//  for(int sector=0;sector<hpdir->m_uiBlockCount;sector++) //BlockCount not consistent with current identify code
  for(int sector=0 ; (sector<33) && readOk ; sector++)
  {
    Serial.print("Read sector ");
    Serial.println(sector);
    hpdir->setUnit(unitToDump);
    hpdir->setVolume(0);
    hpdir->setAddress(sector);
    nDt=256;
    if (hpdir->read(dt,256,nDt)!=0)
    {
        hpdir->getStatus(st,24,nSt);
        readOk=false;
        dump(st,nSt);
    }
    JustHex(dt,nDt);
  }

  if (!readOk)
  {
    Serial.println("Read aborted due to some error");
  }

}

void Format()
{
  Serial.println();
  Serial.print("Are you really really sure? (Press Y)");
  while (!Serial.available());
  int sure;
  sure = Serial.read();
  if (sure == 'Y')
  {
    Serial.println();
    Serial.println();
    Serial.println("Formatting.....");
    hpdir->setUnit(1);
    hpdir->setVolume(0);
    hpdir->initializeMedia();
  }
}

void WriteDisc()
{
  char st[24];
  unsigned int nSt;
  char dt[256];

  Serial.println();
  Serial.print("Are you really really sure? (Press Y)");
  while (!Serial.available());
  int sure;
  sure = Serial.read();
  Serial.println();
  if (sure == 'Y')
  {
    randomSeed(Entropy.random());
    for (uint16_t ptr=0; ptr<256; ptr++)
      dt[ptr]=random(256);

    bool writeOk = true;
    for(int sector=0 ; (sector<3) && writeOk ; sector++)
    {
      if (sector==0)
      {
        for (uint16_t ptr=0; ptr<256; ptr++)
          dt[ptr]=ptr;
      }
      else if (sector==1)
      {
        for (uint16_t ptr=0; ptr<256; ptr++)
          dt[ptr]=random(256);
      }
      else if (sector==2)
      {
        for (uint16_t ptr=0; ptr<256; ptr++)
          dt[ptr]=0x55;
      }
      Serial.print("Write sector ");
      Serial.println(sector);
      hpdir->setUnit(1);
      hpdir->setVolume(0);
      hpdir->setAddress(sector);
      if (hpdir->write(dt,256)!=0)
      {
          hpdir->getStatus(st,24,nSt);
          writeOk=false;
          dump(st,nSt);
      }
    }
  }
}

const char SOH = 1;
const char EOT = 4;
const char ACK = 6;
const char NAK = 21;
const char SUB = 26;

enum events_t {evNone, evTimeOut, evAbort, evSOH, evRxChar, evEOT, evRxBuf, evDiskBuf};

void XModemWrt()
{
  hpdir->setUnit(1);
  hpdir->setVolume(0);
  char dt[38];
  unsigned int nDt;
  hpdir->describe(dt,38,nDt);

  SS80_VolumeDescription* volPtr = (SS80_VolumeDescription*)(dt+24);
  hpdir->m_uiBlockCount = (uint8_t)(dt[24+10])*256+(uint8_t)(dt[24+11])+1;

  Serial.print("Data will be written to unit 1 starting at sector 0 up to max ");
  Serial.print(hpdir->m_uiBlockCount);
  Serial.println(" sectors");
  Serial.println("If more data is received it will be discarded.");
  Serial.println();
  Serial.println("Waiting for XModem transfer to begin");

  uint16_t sectorNum = 0;
  uint16_t packetNum = 1;
  uint16_t retries=10;
  char rxBuf[132];
  uint8_t rxPtr = 0;
  char diskbuf[256];
  uint16_t diskPtr = 0;
  bool rxInit = false;
  uint8_t checksum = 0;
  char st[24];
  unsigned int nSt;
  bool valid;
  uint8_t lastRx = 255;

  unsigned long tmo = millis()-11000; //Ensure direct timeout
  char rxchar=0;
  events_t event = evNone;

  while (event!=evAbort)
  {
    switch (event)
    {
      case evNone:
        if (Serial.available())
        {
          rxchar = Serial.read();
          if (!rxInit)
          {
            if (rxchar==SOH)
              event=evSOH; //This is how it starts
            else
              event=evNone; //Just ignore noise
          }
          else if (rxchar==EOT)
            event=evEOT; //This may be how it ends but thats checked by the event handler
          else
            event=evRxChar;
          tmo=millis();
        }
        else //No char available
        {
          if (millis()>(tmo+10000))
            event=evTimeOut;
        }
        break;

      case evTimeOut:
        //Send <NAK>
        Serial.print((char)(NAK));
        //reset timer
        tmo = millis();
        //Decrement number of tries remaining
        retries--;
        if (retries>0)
          event=evNone;
        else
        {
          event=evAbort;
          Serial.print("Too many retries, aborting");
        }
        break;

      case evAbort:
        //Should be taken care of att event loop level.
        break;

      case evSOH:
        //Set init flag
        rxInit=true;
        //Next event evRxChar. Will put the <SOH> character in the packet buffer even for the first data packet
        event=evRxChar;
        break;

      case evRxChar:
        //Append char to rx buffer
        rxBuf[rxPtr]=rxchar;
        rxPtr++;
        //if # received = 132 next event evRxBuf
        if (rxPtr==132) //Received a full packet
          event=evRxBuf;
        else //Just received a char and added to the buffer
          event=evNone;
        break;

      case evEOT:
        if (rxPtr==0)
        {
          Serial.print((char)(ACK));
          Serial.println("End of file read. Stop receive");
          event=evAbort;
        }
        else
          event=evRxChar;
        break;

      case evRxBuf:
        checksum=0;
        valid=true;
        retries=10;
        //Check buffer framing and checksum (TODO)
        valid=valid && (rxBuf[0]=SOH);
        valid=valid && (rxBuf[1] == (255-rxBuf[2]));
        for (uint8_t i=0; i<128; i++)
          checksum+=rxBuf[i+3]; //Data starts at position 3
        valid=valid && (checksum==rxBuf[131]);
        valid=valid && (rxBuf[1]==(packetNum%256));

        if (valid)
        {
          //Append rx buf to disk buffer
          for(uint8_t i=0;i<128;i++)
            diskbuf[diskPtr+i]=rxBuf[i+3];
          //Clean rx buffer
          rxPtr=0;
          packetNum++;
          //Increase disk buffer pointer
          diskPtr=diskPtr+128;
          //if disk buffer full, next event evDiskBuf else evNone
          if (diskPtr>255)
            event=evDiskBuf;
          else
          {
            event=evNone;
            Serial.print((char)(ACK));
            lastRx=rxBuf[1];
          }
        }
        else
        {
          event=evNone;
          if (rxBuf[1]==lastRx)
            Serial.print((char)(ACK));
          else
            Serial.print((char)(NAK));
        }
        break;

      case evDiskBuf:
        if (sectorNum<hpdir->m_uiBlockCount)
        {
          //Write to disk
          hpdir->setUnit(1);
          hpdir->setVolume(0);
          hpdir->setAddress(sectorNum);
          if (hpdir->write(diskbuf,256)!=0)
          {
            hpdir->getStatus(st,24,nSt);
            dump(st,nSt);
            //next event = evAbort since the write failed
            event=evAbort;
          }
          else
            //next event = evNone
            event=evNone;
        }
        else
          event=evNone;
        sectorNum++;
        //Clean buffers
        for (uint16_t i=0; i<256; i++)
          diskbuf[i]=0;
        diskPtr=0;

        Serial.print((char)(ACK));
        break;
    }
  }
}

int choise;

void loop() {
  char clear[] = "\e[H\e[J";
  Serial.print(clear);
  Serial.println("*******************************************************");
  Serial.println("SS80 lab");
  Serial.println();
  Serial.println("A    Amigo clear");
  Serial.println("C    Clear QStat");
  Serial.println("D    Identify");
  Serial.println("F    Initialize Media in unit 1");
  Serial.println("I    Initiate Diagnostic");
  Serial.println("R    Locate and Read");
  Serial.println("W    Locate and Write in unit 1");
  Serial.println("X    X-Modem receive and write");
  Serial.println();

  bool valid = false;
  while (!valid)
  {
    Serial.print("Select (A/C/D/F/I/R/W/X) : ");

    while (!Serial.available());
    
    choise = Serial.read();
    Serial.println();
    valid = true;
    switch (choise)
    {
      case 'A':
      case 'a':
        hpdir->amigoClear(); break;
      case 'C':
      case 'c':
        ClearQStat(); break;        //Do a fake read on all units to clear QStat=2 (new media)
      case 'D':
      case 'd':
        Identify(); break;            //Gets data about the drive conntected
      case 'F':
        Format(); break;
      case 'I':
      case 'i':
        Diagnostic(); break;          //Runs diagnostics on all units on the connected drive
      case 'R':
      case 'r':
        DumpDisc(); break;            //Reads all sectors on one disk and dumps as hex
      case 'W':
        WriteDisc(); break;
      case 'X':
      case 'x':
        XModemWrt(); break;
      default:
        valid=false;
    }
  }
  Serial.println();
  Serial.println("Press any key to continue");
  while (!Serial.available());
  choise = Serial.read();
}
#endif

#ifdef INSTTEST
IBNODE* hp3478;
IBNODE* hp5316;
IBNODE* hp33120;
IBNODE* PM6680;
IBNODE* tek2431;
IBNODE* wavetek81;

char st[256];
unsigned int nSt;

void setup() {
  while(!Serial)
  {
    ; //Wait for serial monitor
  }
  hp3478=gpib.getInstrument();
  hp3478->setAddr(5);
  
  tek2431=gpib.getInstrument();
  tek2431->setAddr(8);
  PM6680=gpib.getInstrument();
  PM6680->setAddr(11);
  hp33120=gpib.getInstrument();
  hp33120->setAddr(12);

  hp5316=gpib.getInstrument();
  hp5316->setAddr(20);
  wavetek81=gpib.getInstrument();
  wavetek81->setAddr(28);

  gpib.begin();
  Serial.println("*******************************************************");

  //tek2431->write("SET?");
  //tek2431->read(st,256,nSt);

//  hp5316->write("RE");
//  hp5316->read(st,256,nSt);
//  Serial.print(st);
//  hp33120->write("*IDN?\n");
//  hp33120->read(st,256,nSt);
//  Serial.print(st);
//  PM6680->write("*IDN?\n");
//  PM6680->read(st,256,nSt);
//  Serial.println(st);
}

float mult = 1.0594630943592952645618252949463; //Perhaps a few more decimals than relevant for a float...
String s;

void loop() {
//  tstart=millis();
//  wavetek81->write("FRQ 111.1");
//  tstop=millis();
//  Serial.print("Write time ");
//  Serial.println(tstop-tstart);
//  delay(500);
//  wavetek81->write("FRQ 222");
  for (float amp=0.0144;amp<140.0;amp*=10)
  {
    if (amp<1.0)
      s="AMP "+String(amp*1000,3)+"MV;";
    else
      s="AMP "+String(amp,3)+"V;";
    s.toCharArray(st,255);
    wavetek81->write(st);
    s="R-1";
    if (amp>0.3)
      s="R0";
    if (amp>3)
      s="R1";
    s.toCharArray(st,255);
    hp3478->write(st);
    
    delay(500);

    for(float freq=6.5186319719579457111832127382352;freq<=1020000.0;freq*=mult)
    {
      s="FRQ "+String(freq)+";";
      Serial.print(amp,5);
      Serial.print(";");
      Serial.print(freq);
      Serial.print(";");
      s.toCharArray(st,255);
      wavetek81->write(st);

      if (freq<200.0)
        delay(2000);
      else
        delay(500);

      hp3478->read(st,255,nSt);
      st[nSt-1]=0;
      Serial.println(st);
    }
  }
//  delay(500);
//  PM6680->write("READ?");
//  PM6680->read(st,256,nSt);
//  Serial.println(st);

  Serial.println("*******************************************************");
//  wavetek81->write("CST?");
//  delay(1);

//  wavetek81->read(st,256,nSt);
//  hp5316->read(st,256,nSt);
//  Serial.println(st);
  
//  delay(100);
}
#endif
 
void dump(char buf[], unsigned int nSize)
{
  String adr;
  String bytes;
  String text;
  for (unsigned int i=0; i<nSize; i++)
  {
    if ((buf[i]<32)||(buf[i]>126))
      text+='.';
    else
      text+=buf[i];
    if (i%16==0)
    {
      adr=String(i,HEX);
      while (adr.length()<4)
        adr="0"+adr;
      adr+="  ";
    }
    if ((int)(buf[i])<16)
      bytes+="0";
    byte b = (byte)(buf[i]);
    bytes+=String(b,HEX);
    if (i%16==7)
      bytes+="-";
    else
      bytes+=" ";
    if (i%16==15)
    {
      Serial.print(adr);
      Serial.print(bytes);
      Serial.println(text);
      adr="";
      bytes="";
      text="";
    }
  }
  if (text!="")
  {
    Serial.print(adr);
    while (bytes.length()<48)
      bytes+=" ";
    Serial.print(bytes);
    Serial.println(text);
  }
}

void JustHex(char buf[], unsigned int nBufSize)
{
  unsigned int i;
  for (i=0; i<nBufSize; i++)
  {
    //Add a 0 if number is less than 10hex (16 dec) to always get 2 digits per byte
    if ((int)(buf[i])<16)
      Serial.print("0");
    Serial.print(buf[i],HEX);
    
    if (i%16==15)
      Serial.println();
  }
  if (i%16!=0)
    Serial.println();
}
