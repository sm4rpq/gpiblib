/*
Copyright 2022 Leif Holmgren, SM4RPQ

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <arduino.h>

const char GTL =  1; // Go To Local
const char SDC =  4; // Selective Device Clear
const char PPC =  5; // Parallell Poll Configure
const char GET =  8; // Group Execute Trigger
const char TCT =  9; // Take ConTrol (NB! Hardware does not support transfer of control)
const char LLO = 17; // Local Lock Out
const char DCL = 20; // Device CLear
const char PPU = 21; // Parallell Poll Unconfigre
//const char SPE = 24; // Serial Poll Enable
const char SPD = 25; // Serial Poll Disable
const char UNL = 63; // UNListen
const char UNT = 95; // UNTalk

class IBCONTROLLER
{
  public:

    IBCONTROLLER();
    ~IBCONTROLLER();

    void begin();
    void end();
    
    bool write(unsigned int uiPrimaryAddress, unsigned int uiSecondaryAddress, char pBuf[], unsigned int nChars, bool bSetEOI, bool bAddCR = false, bool bAddLF = false);
    bool read(unsigned int uiPrimaryAddress, unsigned int uiSecondaryAddress, char pBuf[], unsigned int nBufSize, unsigned int& nChars, bool bBreakOnEOI, bool bBreakOnCR, bool bBreakOnLF);

    uint8_t ppoll();
    void dcl(unsigned int uiPrimaryAddress, unsigned int uiSecondaryAddress);
    bool srq();
    
  private:
    bool m_bAttention;
    bool m_bTalker;
    void setOutput();
    void setInput();
    bool wbyte(byte b,bool eoi=false);
    bool rbyte(char& rxByte, bool &rxEoi);
    void setAttention(bool b);
    unsigned int m_uiTimeout; //Handshake timeout
};

