/*
Copyright 2022 Leif Holmgren, SM4RPQ

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <arduino.h>

class IBCONTROLLER;

//The describe command returns information about the controoler, the drives and volumes on each drive.
//The following structures show the information returned. Unfortunately the byte order in the drive
//does not match the ARM processor so any field with >8 bits will not be possible to map to a largr
//datatype without problems. For that reason these are declared as arrays of 8-bit datatypes.


struct SS80_ControllerDescription {
  uint8_t installedUnits[2];    // Bit-field showing which units are available. Bit 15 will always be on as it corresponds to the controller.
  uint8_t tfrRate[2];           // Measured transfer rate over gpib (kind of unreliable)
  uint8_t controllerType;       // Enumeration with different types of controllers. (Replace with enum?)
} ;

struct SS80_UnitDescription {
  uint8_t genericUnitType;      // Enumeration for type of drive. 0=Fixed disc, 1=Removable disc
  uint8_t deviceNumber[3];
  uint8_t numBytesPerBlock[2];  // 16-bit entity showing size of a sector/block
  uint8_t numBlockBuffers;      // Number of sector/block buffers in the drive.
  uint8_t recommendedBurstSize; // Will always be 0 for SS80 devices
  uint8_t blockTimeMillis[2];
  uint8_t transferRatekBps[2];
  uint8_t optimalRetryTime[2];
  uint8_t accessTimeMillis[2];
  uint8_t maxInterleave;
  uint8_t fixedVolumeByte;
  uint8_t removableVolumeByte;
} ;

struct SS80_VolumeDescription {
  uint8_t maxVectorAddress[3];  // Kind of unuseable information about absolute geometry since addressing for locate commands are linear...
  uint8_t maxHeadAddress;       // 
  uint8_t maxSectorAddress[2];  // 
  uint8_t maxSingleVectorAddress[6];// 48-bit max address for setAddress
  uint8_t currentInterleave;
} ;

class SS80
{
  public:
    SS80(IBCONTROLLER* interface);
    ~SS80();

    uint32_t m_uiBlockSize;
    uint32_t m_uiBlockCount;

    //Sets the GPIB-address of the device to access
    void setDevice(uint8_t device);

    //Sets the unit number to access (if there are more than one)
    //Unit numbers are 0-based and 15 refers to the controller
    void setUnit(uint8_t unit);

    //Sets the volume of the unit. Not used for floppies or tapes
    //but used for fixed discs. Kind of corresponds to partitions
    //in the PC-world
    void setVolume(uint8_t volume);

    //Sets the BLOCK to start reading at. 
    //NB! Linear addressing 0-maxSingleVectorAddress as returned from describe
    //Describe returns a 48-bit number and the setAddress command allows it but
    //none of the devices of interest supports more than 4G blocks anyway so relaxed
    //to a 32-bit number in this call.
    void setAddress(uint32_t address);

    //Sends a identify command to the selected device. Return value is the second
    //byte of what the drive responds. (The first byte is alwats 80)
    uint8_t identify();

    //Retrieves the description from the selected device into the passed buffer.
    //Use setUnit and setVolume before calling this function to direct the
    //describe command correctly.
    uint8_t describe(char pBuf[], unsigned int nBufSize, unsigned int & nChars);

    //Formats the media using default format. This will not place a
    //filesystem on the disc. Just prepare it with indexing information
    //All blocks will contain 0 after this operation.
    //Use setUnit and setVolume before calling this function to direct the
    //initializeMedia command correctly.
    void initializeMedia();

    //Read a block of specified size from the selected device/unit/volume
    //starting at the block specified. If successive calls to read are
    //made without calling any of the setUnit/Volume/Address functions the
    //read will continue from where the last one was done.
    uint8_t read(char pBuf[], unsigned int nBufSize, unsigned int & nSize);

    //Write a block of specified size to the selected device/unit/volume
    //starting at the block specified. If successive calls to write are
    //made without calling any of the setUnit/Volume/Address functions the
    //write will continue from where the last one was done.
    uint8_t write(char pBuf[], unsigned int nSize);

    //Request full status from the device. Interprets it and
    //keeps the result for future call to getStatus.
    uint8_t requestStatus();

    //Get the full status from the last requestStatus towards the
    //device
    void getStatus(char buf[], unsigned int nBufSize, unsigned int & nSize);

    //Sends an amigo clear command and waits for the device to be ready
    void amigoClear();

    //Sends a channel independent clear command to the device and waits
    //for the device to be ready.
    uint8_t channelIndepClear();

    //Initiates a diagnostic in the device.
    //Set unit to n or 15 before calling
    uint8_t initDiagnostic();

  private:
    uint8_t m_uiDevice;
    uint8_t m_uiDeviceMask;
    bool m_bSetUnit;
    uint8_t m_uiUnit;
    bool m_bSetVolume;
    uint8_t m_uiVolume;
    bool m_bSetAddress;
    uint32_t m_uiAddress;

    char m_status[24];
    unsigned int m_nStatusSize;
    
    IBCONTROLLER* m_pInterface;

    //Sends command part of a device transaction
    void command(char cmd[], unsigned int nCmdSize);

    //Executes the read from the device
    void executeRead(char pBuf[], unsigned int nBufSize, unsigned int & nChars);
    
    //Executes the rite to the device
    void executeWrite(char pBuf[], unsigned int nChars);

    //Sends report command and reads the qstat back.
    //Returns the qstat value
    uint8_t report();
};

