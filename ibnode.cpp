/*
Copyright 2022 Leif Holmgren, SM4RPQ

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ibnode.h"
#include "ibcontroller.h"

IBNODE::IBNODE(IBCONTROLLER* interface)
{
  m_bTxEOI=true;
  m_bRxEOI=true;
  m_bAppendCR=true;
  m_bAppendLF=true;
  m_bTermOnCR=false;
  m_bTermOnLF=true;
  m_pController=interface;
}

IBNODE::~IBNODE()
{
  
}

void IBNODE::setTxEOI(bool bUseEOI)
{
  m_bTxEOI=bUseEOI; 
}

void IBNODE::setRxEOI(bool bUseEOI)
{
  m_bRxEOI=bUseEOI;
}
    
void IBNODE::setTxTerm(bool bAppendCR, bool bAppendLF)
{
  m_bAppendCR=bAppendCR;
  m_bAppendLF=bAppendLF;
}

void IBNODE::setRxTerm(bool bTermOnCR, bool bTermOnLF)
{
  m_bTermOnCR=bTermOnCR;
  m_bTermOnLF=bTermOnLF;
}

void IBNODE::setAddr(uint8_t iPrimary, uint8_t iSecondary)
{
  m_uiPrimaryAddr=iPrimary;
  m_uiSecondaryAddr=iSecondary;
}

bool IBNODE::write(char pBuf[])
{
  return m_pController->write(m_uiPrimaryAddr, m_uiSecondaryAddr, pBuf, strlen(pBuf), m_bTxEOI, m_bAppendCR, m_bAppendLF);
}

bool IBNODE::write(char pBuf[], unsigned int nChars)
{
  return m_pController->write(m_uiPrimaryAddr, m_uiSecondaryAddr, pBuf, nChars, m_bTxEOI);
}

bool IBNODE::read(char pBuf[], unsigned int nBufSize, unsigned int & nChars)
{
  return m_pController->read(m_uiPrimaryAddr,m_uiSecondaryAddr,pBuf,nBufSize,nChars,m_bRxEOI,m_bTermOnCR,m_bTermOnLF);
}

